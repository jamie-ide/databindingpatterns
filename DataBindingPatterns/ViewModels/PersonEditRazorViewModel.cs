﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DataBindingPatterns.ViewModels
{
    public class PersonEditRazorViewModel
    {
        [Required]
        public int PersonId { get; set; }

        [Required, StringLength(50)]
        public string FirstName { get; set; }

        [Required, StringLength(50)]
        public string LastName { get; set; }

        [Required, StringLength(50)]
        public string FavoriteFood { get; set; }

        // Right away this feels wrong because I am mixing data for a select with model data including validation attributes
        public IEnumerable<SelectListItem> Foods { get; set; } 
    }
}