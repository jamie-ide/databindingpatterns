﻿namespace DataBindingPatterns.ViewModels
{
    public class PersonViewModel
    {
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public string FavoriteFood { get; set; }
    }
}