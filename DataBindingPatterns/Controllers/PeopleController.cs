﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataBindingPatterns.Models;
using DataBindingPatterns.ViewModels;

namespace DataBindingPatterns.Controllers
{
    public abstract class PeopleController : Controller
    {
        protected IEnumerable<Person> GetPeople()
        {
            return new List<Person>
            {
                new Person(1, "Alexander", "Hamilton", "Boeuf a la mode"),
                new Person(2, "Amadeus", "Mozart", "Faschingkrapfen"),
                new Person(3, "Marco", "Polo", "Martarolo"),
                new Person(4, "Helen", "Keller", "French fries")
            };
        }

        protected IEnumerable<string> GetFood()
        {
            return new[] { "Cheeseburger", "Roast chicken", "Veggie stir fry" };
        }

        protected   IEnumerable<PersonViewModel> GetPersonViewModels()
        {
            return GetPeople()
                .Select(x => new PersonViewModel()
                {
                    PersonId = x.PersonId,
                    FullName = x.FirstName + " " + x.LastName,
                    FavoriteFood = x.FavoriteFood
                });
        }

        protected PersonViewModel GetPersonViewModel(int id)
        {
            return GetPeople()
                .Where(x => x.PersonId == id)
                .Select(x => new PersonViewModel()
                {
                    PersonId = x.PersonId,
                    FullName = x.FirstName + " " + x.LastName,
                    FavoriteFood = x.FavoriteFood
                }).SingleOrDefault();
        }

    }
}