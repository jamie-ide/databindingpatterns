﻿using System;
using System.Linq;
using System.Web.Mvc;
using DataBindingPatterns.ViewModels;

namespace DataBindingPatterns.Controllers
{
    // straight razor
    public class People1Controller : PeopleController
    {

        [HttpGet]
        public ActionResult Index()
        {
            var vm = GetPersonViewModels();
            return View(vm);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var vm = GetPeople()
                .Where(x => x.PersonId == id)
                .Select(x => new PersonEditRazorViewModel()
                {
                    PersonId = x.PersonId,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    FavoriteFood = x.FavoriteFood
                }).SingleOrDefault();

            if (vm == null)
            {
                return HttpNotFound();
            }

            SetFoodList(vm);

            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(PersonEditRazorViewModel vm)
        {
            var food = GetFood();
            if (!food.Contains(vm.FavoriteFood))
            {
                var msg = string.Format("'{0}' is not in the list '{1}'.", vm.FavoriteFood, string.Join(", ", food));
                ModelState.AddModelError("Favorite Food", msg);
            }

            // Have to rebuild select list if validation or update fails
            if (!ModelState.IsValid)
            {
                SetFoodList(vm);
                return View("Edit", vm);
            }

            try
            {
                // update
            }
            catch (Exception)
            {
                // rebuild select list and redirect to edit (if appropriate)
                throw;
            }

            return RedirectToAction("Index");
        }

        private void SetFoodList(PersonEditRazorViewModel vm)
        {
            var foods = GetFood().ToList();

            // add existing favorite if not in list
            if (!foods.Contains(vm.FavoriteFood))
            {
                foods.Add(vm.FavoriteFood);
            }

            vm.Foods = foods.Select(x => new SelectListItem()
            {
                Text = x,
                Value = x,
                Selected = (x == vm.FavoriteFood)
            });
        }

    }
}