﻿using System.Web.Mvc;

namespace DataBindingPatterns.Controllers
{
    public class People3Controller : PeopleController
    {

        public ActionResult Index()
        {
            var vm = GetPersonViewModels();
            return View(vm);
        }
    }
}