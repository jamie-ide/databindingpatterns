﻿namespace DataBindingPatterns.Models
{
    public class Person
    {
        public Person(int personId, string firstName, string lastName, string favoriteFood)
        {
            PersonId = personId;
            FirstName = firstName;
            LastName = lastName;
            FavoriteFood = favoriteFood;
        }

        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FavoriteFood { get; set; }
    }
}